Installing and Configuring Natlink, including Unimacro and Vocola

Please read the pages in this section of the website and follow the install and configure instructions.

-Start with the link(installation, Installation).
-Proceed with the link(Configuration)

If problems arise, please read through the other pages in the left menu.

If problems persist, ask mailto(q.hoogenboom@antenna.nl, Quintijn Hoogenboom).